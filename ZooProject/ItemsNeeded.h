//
// Created by Adam on 11/12/2019.
//

#ifndef ZOOPROJECT_ITEMSNEEDED_H
#define ZOOPROJECT_ITEMSNEEDED_H


#include "Item.h"
#include <vector>

class ItemsNeeded {
private:
    Item * m_itemNeeded;
    float m_amount;
public:
    ItemsNeeded(Item * itemNeeded, float amount);
    Item * getItemNeeded();
    float getAmountNeeded();
};


#endif //ZOOPROJECT_ITEMSNEEDED_H
