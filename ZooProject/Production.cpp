//
// Created by Adam on 04/12/2019.
//

#include "Company.h"
#include "Production.h"
#include <string>
extern float* cashPtr;
extern Storage* storagePtr;
extern Company* companyPtr;

Production::Production() {
    m_hoursWorked=0;
}

void Production::makeProduct(const string& productName, float amount) {
    if(amount<=0){
        cout << "Wrong input! try again." << endl;
    } else {
        Product *productVar = storagePtr->getProduct(productName);
        bool haveEnoughResources = true;
        for(auto i : productVar->getItemsNeeded()) {
            if (amount * i->getAmountNeeded() > i->getItemNeeded()->getAmount()) {
                haveEnoughResources = false;
                break;
            }
        }
        if(productVar->getTimeNeeded()*amount > getRemainingWorkTime() || !haveEnoughResources){
            cout << "You don't have enough machine work time or resources to make that many!" << endl;
            cout << "WorkTime needed: " << amount*productVar->getTimeNeeded() << "h || ";
            cout << "WorkTime available: " << getRemainingWorkTime() << "h " << endl;
            for(auto i : productVar->getItemsNeeded()){
                cout << i->getAmountNeeded()*amount << " " << i->getItemNeeded()->getName() << " is needed. ";
                cout << "You have " << i->getItemNeeded()->getAmount() << " " << i->getItemNeeded()->getName() << endl;
            }
        }else{
            for (auto i : productVar->getItemsNeeded()) {
                i->getItemNeeded()->setAmount((-1) * amount * i->getAmountNeeded());
            }
            productVar->setAmount(amount);
            m_hoursWorked += amount * productVar->getTimeNeeded();
        }
    }
}

float Production::getHoursWorked() {
    return m_hoursWorked;
}

float Production::getRemainingWorkTime() {
    float remainingHoursVar = 0;
    for(auto i : m_machines){
        remainingHoursVar += (i->getProductionRate()*i->getAmount())-getHoursWorked();
    }
    return remainingHoursVar;
}

float Production::getTotalWorkTime() {
    float remainingHoursVar = 0;
    for(auto i : m_machines){
        remainingHoursVar += i->getProductionRate()*i->getAmount();
    }
    return remainingHoursVar;
}

class Machine * Production::getMachine(const string& machineName) {
    for(auto i : m_machines){
        if(i->getName()==machineName){
            return i;
        }
    }
    return nullptr;
}

void Production::buyMachine(const string& name, int amount) {
    if(amount<=0){
        cout << "Wrong input! try again." << endl;
    } else {
        auto varMachine = getMachine(name);
        if (varMachine->getPrice() * amount > *cashPtr) {
            cout << "You don't have enough money to buy that machine!" << endl;
        } else {
            companyPtr->setCash((-1) * varMachine->getPrice());
            varMachine->setAmount(amount);
        }
    }
}

void Production::sellMachine(string name, int amount) {
    if(amount<=0){
        cout << "Wrong input! try again." << endl;
    } else {
        auto varMachine = getMachine(name);
        companyPtr->setCash(varMachine->getPrice() / 2);
        varMachine->setAmount((-1) * amount);
    }
}
