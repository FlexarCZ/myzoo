//
// Created by Adam on 08/12/2019.
//

#include <iostream>
#include <string>
#include "InputScan.h"

extern Storage* storagePtr;
extern Production* productionPtr;
extern Company* companyPtr;

vector<string> InputScan::splitInput(const string& playerInput) {
    string word;
    vector<string> splitInput = {};
    for (auto x : playerInput){
        if (x == ' '){
            splitInput.push_back(word);
            word = "";
        }else word += x;
    }
    splitInput.push_back(word);
    return splitInput;
}

void InputScan::processInput(vector<string> splitInput) {
    if(splitInput[0]!="exit" && splitInput[0]!="buy" && splitInput[0]!="sell" && splitInput[0]!="change" && splitInput[0]!="make" && splitInput[0]!="info" && splitInput[0]!="help"){
        cout << "Wrong input!" << endl;
    }else if(splitInput[0]=="buy"){
        //******** BUY ************//
        if(splitInput.size()==3 && storagePtr->getItem(splitInput[1])){
            storagePtr->buyItem(splitInput[1], stof(splitInput[2]));
        }else if(splitInput.size()==3 && productionPtr->getMachine(splitInput[1])){
            productionPtr->buyMachine(splitInput[1], stoi(splitInput[2]));
        }else cout << "Wrong input/Item not found!" << endl;
    }else if(splitInput[0]=="sell"){
        //******** SELL ************//
        if(splitInput.size()==3 && storagePtr->getItem(splitInput[1])){
            storagePtr->sellItem(splitInput[1], stof(splitInput[2]));
        }else if(splitInput.size()==3 && productionPtr->getMachine(splitInput[1])){
            productionPtr->sellMachine(splitInput[1], stoi(splitInput[2]));
        }else cout << "Wrong input/Item/machine not found!" << endl;
    }else if(splitInput[0]=="change"){
        //******** CHANGE **********//
        //TODO CHANGE
    }else if(splitInput[0]=="make"){
        if(splitInput.size()==3 && storagePtr->getProduct(splitInput[1])){
            productionPtr->makeProduct(splitInput[1],stof(splitInput[2]));
        } else cout << "Wrong input/product not found!" << endl;
    }else if(splitInput[0]=="help"){
        cout << "# buy [what] [how much] - to buy stuff" << endl << "# sell [what] [how much] - to sell stuff" << endl << "# change - TODO" << endl;
        cout << "# make [what] [how much] - to make stuff" << endl << "# info - to print info" << endl;
    }else if (splitInput[0]=="info"){
        companyPtr->printInfo();
    }
}