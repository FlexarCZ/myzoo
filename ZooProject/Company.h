//
// Created by Adam on 04/12/2019.
//

#ifndef ZOOPROJECT_COMPANY_H
#define ZOOPROJECT_COMPANY_H

#include <iostream>
using namespace std;
#include "Production.h"
#include "Storage.h"



class Company {
    float m_cash;
public:
    static Storage* s_storage;
    static Production* s_production;
    Company(float cash);
    float getCash();
    void setCash(float difference);

    void printInfo();
};


#endif //ZOOPROJECT_COMPANY_H
