//
// Created by Adam on 04/12/2019.
//

#ifndef ZOOPROJECT_MACHINE_H
#define ZOOPROJECT_MACHINE_H

#include <iostream>
using namespace std;

class Machine {
    string m_name;
    float m_productionRate;
    float m_price;
    float m_costPerRound;
    int m_amount;
public:
    Machine(string name, float productionRate, float price, float costPerHour);
    float getProductionRate();
    float getPrice();
    string getName();
    int getAmount();
    float getCostPerRound();
    void setAmount(int difference);
    void printInfo();
};


#endif //ZOOPROJECT_MACHINE_H
