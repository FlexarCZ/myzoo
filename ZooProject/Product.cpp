//
// Created by Adam on 04/12/2019.
//

#include "Product.h"


Product::Product(string name, float price, vector<ItemsNeeded *> itemsNeeded, float timeNeeded, float spaceRequired) : Item(name, price, 0, spaceRequired) {
    m_timeNeeded = timeNeeded;
    m_spaceRequired = spaceRequired;
    m_itemsNeeded = itemsNeeded;
}

float Product::getTimeNeeded() {
    return m_timeNeeded;
}

vector<class ItemsNeeded *> Product::getItemsNeeded() {
    return m_itemsNeeded;
}
