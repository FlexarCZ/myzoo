//
// Created by Adam on 04/12/2019.
//

#include "Storage.h"
#include "Company.h"

extern float* cashPtr;
extern Company* companyPtr;

Storage::Storage(){
    m_capacity = 500;
    m_capacityMultiplier = 1;
    m_usedCapacity = 0;
}

void Storage::sellItem(const string& itemName, float amount) {
    if(amount<=0) {
        cout << "Wrong input! try again." << endl;
    } else {
        Item* itemPtr = getItem(itemName);

        if (amount > itemPtr->getAmount()) {
            cout << "Insufficient resources!" << endl;
        } else {
            itemPtr->setAmount((-1) * amount);
            companyPtr->setCash(itemPtr->getPrice() * amount);
        }
    }
}

void Storage::buyItem(std::string itemName, float amount) {
    if(amount<=0) {
        cout << "Wrong input! try again." << endl;
    } else {
        Item *itemPtr = getItem(itemName);

        if (itemPtr->getPrice() * amount > *cashPtr) {
            cout << "You don't have enough money, bitch!" << endl;
        } else if (itemPtr->getSpaceRequired() * amount > getRemainingCapacity()) {
            cout << "You don't have enough space for that amount, bitch!" << endl;
        } else {
            itemPtr->setAmount(amount);
            companyPtr->setCash((-1) * (itemPtr->getPrice() * amount));
        }
    }
}

float Storage::getTotalCapacity() {
    return m_capacity*m_capacityMultiplier;
}

float Storage::getRemainingCapacity() {
    return (m_capacity*m_capacityMultiplier)-m_usedCapacity;
}

class Material* Storage::getMaterial(const std::string& materialName) {
    for(auto i : m_materials) {
        if (i->getName() == materialName) {
            return i;
        }
    }
    return nullptr;
}

class Product * Storage::getProduct(const std::string & productName) {
    for(auto i : m_products) {
        if (i->getName() == productName) {
            return i;
        }
    }
    return nullptr;
}

class Item * Storage::getItem(const std::string& itemName) {
    vector<Item *> allItems = {};
    for(auto i : m_materials){
        allItems.push_back(i);
    }
    for(auto i : m_products){
        allItems.push_back(i);
    }
    for(auto i : allItems) {
        if (i->getName() == itemName) {
            return i;
        }
    }
    return nullptr;
}

void Storage::changeUsedCapacity(float difference) {
    m_usedCapacity += difference;
}

void Storage::increaseTotalCapacity(int amount) {
    m_capacityMultiplier += amount;
}
