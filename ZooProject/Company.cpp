//
// Created by Adam on 04/12/2019.
//

#include "Company.h"


float* cashPtr = nullptr;
Storage* Company::s_storage = new Storage();
Production* Company::s_production = new Production();
Storage* storagePtr = nullptr;
Production* productionPtr = nullptr;

Company::Company(float cash) {
    m_cash = cash;
    cashPtr = &m_cash;
    storagePtr = s_storage;
    productionPtr = s_production;
}

void Company::setCash(float difference) {
    m_cash += difference;
}

float Company::getCash() {
    return m_cash;
}

void Company::printInfo() {
    cout << "Company has: " << m_cash << "$." << endl;
    cout << "Storage capacity: " << storagePtr->getTotalCapacity()-storagePtr->getRemainingCapacity() << "/" << storagePtr->getTotalCapacity() << " - " << (storagePtr->getTotalCapacity()-storagePtr->getRemainingCapacity())/storagePtr->getTotalCapacity()*100 << "% full" <<  endl;
    cout << "Production work time: " << productionPtr->getRemainingWorkTime() << "/" << productionPtr->getTotalWorkTime() << endl;
    cout << "Resources: " << endl;
    for(auto i : s_storage->m_materials){
        i->printInfo();
    }
    cout << "Products: " << endl;
    for(auto i : s_storage->m_products){
        i->printInfo();
    }
    cout << "Machines: " << endl;
    for(auto i : s_production->m_machines){
        i->printInfo();
    }
}