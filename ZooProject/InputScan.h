//
// Created by Adam on 08/12/2019.
//

#ifndef ZOOPROJECT_INPUTSCAN_H
#define ZOOPROJECT_INPUTSCAN_H

#include <vector>
#include "Company.h"
#include "Production.h"
#include "Storage.h"
using namespace std;

class InputScan {
public:
    vector<string> splitInput(const string& playerInput);
    void processInput(vector<string> splitInput);
};

#endif //ZOOPROJECT_INPUTSCAN_H
