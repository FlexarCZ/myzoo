//
// Created by Adam on 04/12/2019.
//

#ifndef ZOOPROJECT_ITEM_H
#define ZOOPROJECT_ITEM_H

#include <iostream>

using namespace std;

class Item {
protected:
    string m_name;
    float m_price;
    float m_amount;
    float m_spaceRequired;
public:
    Item(string name, float price, float amount, float spaceRequired);
    string getName();
    float getPrice();
    float getAmount();
    float getSpaceRequired();
    void setAmount(float difference);
    void printInfo();
};


#endif //ZOOPROJECT_ITEM_H
