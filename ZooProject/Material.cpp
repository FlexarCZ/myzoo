//
// Created by Adam on 04/12/2019.
//

#include "Material.h"

Material::Material(string name, float price, float spaceRequired) : Item(name, price, 0, spaceRequired) {
    m_name = name;
    m_price = price;
    m_spaceRequired = spaceRequired;
}

void Material::setPrice(float newPrice) {
    m_price = newPrice;
}