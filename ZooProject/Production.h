//
// Created by Adam on 04/12/2019.
//

#ifndef ZOOPROJECT_PRODUCTION_H
#define ZOOPROJECT_PRODUCTION_H


#include "Product.h"
#include "Machine.h"

class Production {
    float m_hoursWorked;
public:
    vector<Machine *> m_machines;
    Production();
    void makeProduct(const string& productName, float amount);
    float getHoursWorked();
    void buyMachine(const string& name, int amount);
    void sellMachine(string name, int amount);
    float getRemainingWorkTime();
    float getTotalWorkTime();
    Machine* getMachine(const string& machineName);
};


#endif //ZOOPROJECT_PRODUCTION_H
