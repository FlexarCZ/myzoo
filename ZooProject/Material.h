//
// Created by Adam on 04/12/2019.
//

#ifndef ZOOPROJECT_MATERIAL_H
#define ZOOPROJECT_MATERIAL_H

#include <iostream>
#include "Item.h"

using namespace std;

class Material : public Item {
public:
    Material(string name, float price, float spaceRequired);
    void setPrice(float newPrice);
};

#endif //ZOOPROJECT_MATERIAL_H
